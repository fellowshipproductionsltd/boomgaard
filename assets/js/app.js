(function() {
    var width = 0;
    function updateWindowSize() {
        if (document.body && document.body.offsetWidth) {
            width = document.body.offsetWidth;
        }
        if (document.compatMode=='CSS1Compat' &&
            document.documentElement &&
            document.documentElement.offsetWidth ) {
            width = document.documentElement.offsetWidth;
        }
        if (window.innerWidth) {
            width = window.innerWidth;
        }
    }
    function productSummary(){
        var summary = document.getElementById('product-summary');
        var summaryHTML = summary.innerHTML;
        var altSummary = document.querySelector('.product-summary');
        if(parseInt(width) <= 415) {            
            summary.style = 'display:none;';            
            altSummary.style = 'display:block;';
            altSummary.innerHTML = summaryHTML;
        }
        else{
            summary.style = ''; 
            altSummary.style = 'display:none;';
            altSummary.innerHTML = '';
        }        
    }
    window.onresize = function(event) {
        updateWindowSize();
        productSummary();
    }    
    updateWindowSize();
    productSummary();
 })();